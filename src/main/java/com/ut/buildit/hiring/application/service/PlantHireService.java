package com.ut.buildit.hiring.application.service;

import com.ut.buildit.hiring.application.dto.ExtensionDto;
import com.ut.buildit.hiring.application.dto.PlantHireDto;
import com.ut.buildit.hiring.domain.Status;

import java.util.List;

public interface PlantHireService {


    PlantHireDto savePlantHireRequest(PlantHireDto plantHireDto);

    PlantHireDto getPlantHireRequest(Long id);

    void updateStatus(Status status, Long id, String comment, String serverName);

    PlantHireDto updatePlantHireRequest(PlantHireDto plantHireDto);

    List<PlantHireDto> getAllHires(Long constructionSiteId);

    PlantHireDto updateBasedOnPurchaseOrderStatus(Long purchaseOrderId, Status status);

    void extendPhr(ExtensionDto extensionDto, String serverName);

}
