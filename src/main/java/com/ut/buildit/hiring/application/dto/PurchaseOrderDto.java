package com.ut.buildit.hiring.application.dto;

import lombok.Data;

@Data
public class PurchaseOrderDto {

    Long _id;
    PlantDto plant;
    BusinessPeriodDTO rentalPeriod;
    String userEmail;
    String status;
}
