package com.ut.buildit.hiring.application.service;

import com.ut.buildit.common.domain.BusinessPeriod;
import com.ut.buildit.common.exception.BaseException;
import com.ut.buildit.common.exception.PhrNotFoundException;
import com.ut.buildit.common.service.ApplicationUserService;
import com.ut.buildit.common.service.RentItDaoLocator;
import com.ut.buildit.hiring.application.dto.ExtensionDto;
import com.ut.buildit.hiring.application.dto.PlantHireDto;
import com.ut.buildit.hiring.domain.ConstructionSite;
import com.ut.buildit.hiring.domain.Plant;
import com.ut.buildit.hiring.domain.PlantHireEntity;
import com.ut.buildit.hiring.domain.Status;
import com.ut.buildit.hiring.repositories.ConstructionSiteRepository;
import com.ut.buildit.hiring.repositories.PlantHireRepository;
import com.ut.buildit.hiring.repositories.PlantRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
@Slf4j
public class PlantHireServiceImpl implements PlantHireService {

    @Autowired
    private PlantHireAssembler plantHireAssembler;

    @Autowired
    private PlantHireRepository plantHireRepository;

    @Autowired
    private ConstructionSiteRepository constructionSiteRepository;

    @Autowired
    private PlantRepository plantRepository;

    @Autowired
    private RentItDaoLocator locator;


    @Autowired
    private ApplicationUserService applicationUserService;


    @Override
    public PlantHireDto savePlantHireRequest(PlantHireDto plantHireDto) {

        PlantHireEntity plantHireEntity = new PlantHireEntity();
        ConstructionSite constructionSite = constructionSiteRepository.findById(plantHireDto.getConstructionSiteId())
                .orElse(new ConstructionSite());
        log.warn("construction site, {}", constructionSite);
        if (constructionSite.getId() == null) {
            constructionSite = constructionSiteRepository.saveAndFlush(new ConstructionSite(plantHireDto.getConstructionSiteId()));
        }
        plantHireEntity.setConstructionSite(constructionSite);
        plantHireEntity.setPlantSupplier(plantHireDto.getPlantSupplier());
        Plant plant = plantRepository.findByPlantId(plantHireDto.getPlantId());
        if (plant == null) {
            plant = plantRepository.saveAndFlush(new Plant(plantHireDto.getPlantId(),
                    plantHireDto.getPlantName(), plantHireDto.getPlantPrice()));
        }
        plantHireEntity.setPlant(plant);
        plantHireEntity.setPeriod(BusinessPeriod.of(plantHireDto.getStartDate(), plantHireDto.getEndDate()));
        plantHireEntity.setTotalCost(plantHireDto.getPlantPrice().multiply(new BigDecimal(
                ChronoUnit.DAYS.between(plantHireDto.getStartDate(), plantHireDto.getEndDate()) + 1)));
        plantHireEntity.setSiteEngineerName(plantHireDto.getSiteEngineerName());
        plantHireEntity.setStatus(Status.PENDING.name());
        plantHireEntity.setServerName(plantHireDto.getServerName());

        plantHireEntity.setUser(applicationUserService.findByUser());
        plantHireEntity = plantHireRepository.saveAndFlush(plantHireEntity);
        return plantHireAssembler.toResource(plantHireEntity);
    }

    @Override
    public PlantHireDto getPlantHireRequest(Long id) {
        PlantHireEntity plantHireEntity = plantHireRepository.findById(id).orElse(null);
        if (plantHireEntity == null) {
            throw new PhrNotFoundException(id);
        }
        return plantHireAssembler.toResource(plantHireEntity);
    }

    @Override
    public void updateStatus(Status status, Long id, String comment, String serverName) {
        PlantHireEntity plantHireEntity = plantHireRepository.findById(id).orElse(null);
        if (plantHireEntity == null) {
            throw new PhrNotFoundException(id);
        }
        if (status == Status.APPROVED_BY_WORK_ENG) {
            locator.getRentItDao(serverName).createPurchaseOrder(plantHireEntity);
            plantHireEntity.setStatus(status.name());
        }

        if(status == Status.CANCELED_BY_CLIENT) {
            if(plantHireEntity.getPeriod().getStartDate().isEqual( LocalDate.now()) ||
                    plantHireEntity.getPeriod().getStartDate().isBefore( LocalDate.now())) {
                throw new BaseException("Cannot Cancel Plant Hire request.");
            }
            if(plantHireEntity.getPurchaseOrder() != null) {
                locator.getRentItDao(serverName).CancelPlantHireRequest(plantHireEntity);
            }
        }

        if(status == Status.REJECTED_BY_WORK_ENG) {
            plantHireEntity.setStatus(status.name());
        }
        plantHireEntity.setComment(comment);
        plantHireRepository.saveAndFlush(plantHireEntity);
    }

    @Override
    public PlantHireDto updatePlantHireRequest(PlantHireDto plantHireDto) {
        PlantHireEntity plantHireEntity = plantHireRepository.findById(plantHireDto.get_id()).orElse(null);
        if (plantHireEntity == null) {
            throw new PhrNotFoundException(plantHireDto.get_id());
        }
        ConstructionSite constructionSite = constructionSiteRepository.findById(plantHireDto.getConstructionSiteId())
                .orElse(new ConstructionSite(plantHireDto.getConstructionSiteId()));
        if (constructionSite.getId() == null) {
            constructionSite = constructionSiteRepository.saveAndFlush(new ConstructionSite(plantHireDto.getConstructionSiteId()));
        }
        plantHireEntity.setConstructionSite(constructionSite);
        plantHireEntity.setPlantSupplier(plantHireDto.getPlantSupplier());
        Plant plant = plantRepository.findByPlantId(plantHireDto.getPlantId());
        if (plant == null) {
            plant = plantRepository.saveAndFlush(new Plant(plantHireDto.getPlantId(),
                    plantHireDto.getPlantName(), plantHireDto.getPlantPrice()));
        }
        plantHireEntity.setPlant(plant);
        plantHireEntity.setPeriod(BusinessPeriod.of(plantHireDto.getStartDate(), plantHireDto.getEndDate()));
        plantHireEntity.setSiteEngineerName(plantHireDto.getSiteEngineerName());
        plantHireEntity.setComment(plantHireDto.getComment());
        plantHireEntity = plantHireRepository.saveAndFlush(plantHireEntity);
        return plantHireAssembler.toResource(plantHireEntity);
    }

    @Override
    public List<PlantHireDto> getAllHires(Long constructionSiteId) {
        return plantHireAssembler.toResources(plantHireRepository.findByConstructionSite_Id(constructionSiteId));
    }

    @Override
    public PlantHireDto updateBasedOnPurchaseOrderStatus(Long purchaseOrderId, Status status) {
        PlantHireEntity plantHireEntity = plantHireRepository.findByPurchaseOrder_PurchaseOrderId(purchaseOrderId);
        if (plantHireEntity == null) {
            throw new PhrNotFoundException(purchaseOrderId);
        }
        plantHireEntity.setStatus(status.name());
        plantHireEntity = plantHireRepository.saveAndFlush(plantHireEntity);
        return plantHireAssembler.toResource(plantHireEntity);
    }

    @Override
    public void extendPhr(ExtensionDto extensionDto, String serverName) {
        PlantHireEntity plantHireEntity = plantHireRepository.findByPurchaseOrder_PurchaseOrderId(extensionDto.getPlantHireRequestId());
        if (plantHireEntity == null) {
            throw new PhrNotFoundException(extensionDto.getPlantHireRequestId());
        }
        locator.getRentItDao(serverName).ExtendPO(extensionDto, plantHireEntity.getPurchaseOrder().getPurchaseOrderId());
    }
}
