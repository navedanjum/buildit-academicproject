package com.ut.buildit.hiring.application.service;

import com.ut.buildit.common.exception.BaseException;
import com.ut.buildit.common.service.RentItDaoLocator;
import com.ut.buildit.hiring.application.dto.PlantHireDto;
import com.ut.buildit.hiring.application.dto.PurchaseOrderDto;
import com.ut.buildit.hiring.domain.PlantHireEntity;
import com.ut.buildit.hiring.domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantHireAssembler extends ResourceAssemblerSupport<PlantHireEntity, PlantHireDto> {

    @Autowired
    private RentItDaoLocator locator;

    public PlantHireAssembler() {
        super(PlantHireEntity.class, PlantHireDto.class);
    }

    @Override
    public PlantHireDto toResource(PlantHireEntity plantHireEntity) {
        PlantHireDto plantHireDto = new PlantHireDto();
        plantHireDto.set_id(plantHireEntity.getId());
        plantHireDto.setConstructionSiteId(plantHireEntity.getConstructionSite().getId());
        plantHireDto.setStartDate(plantHireEntity.getPeriod().getStartDate());
        plantHireDto.setEndDate(plantHireEntity.getPeriod().getEndDate());
        plantHireDto.setPlantId(plantHireEntity.getPlant().getPlantId());
        plantHireDto.setPlantName(plantHireEntity.getPlant().getName());
        plantHireDto.setPlantSupplier(plantHireEntity.getPlantSupplier());
        plantHireDto.setSiteEngineerName(plantHireEntity.getSiteEngineerName());
        plantHireDto.setTotalCost(plantHireEntity.getTotalCost());
        plantHireDto.setPurchaseOrderId(plantHireEntity.getPurchaseOrder() == null ? null : plantHireEntity.getPurchaseOrder().getPurchaseOrderId());
        if (plantHireEntity.getStatus() == Status.APPROVED_BY_WORK_ENG.name()) {
            PurchaseOrderDto purchaseOrderDto = locator.getRentItDao(plantHireEntity.getServerName()).getPurchaseOrder(plantHireEntity.getPurchaseOrder().getPurchaseOrderId());
            if (purchaseOrderDto == null) {
                throw new BaseException("No Purchase Order found at rentIT");
            }
            plantHireDto.setStatus(purchaseOrderDto.getStatus());
        } else {
            plantHireDto.setStatus(plantHireEntity.getStatus());
        }
        plantHireDto.setServerName(plantHireEntity.getServerName());
        plantHireDto.setComment(plantHireEntity.getComment());
        plantHireDto.setPlantPrice(plantHireEntity.getPlant().getPrice());
        return plantHireDto;
    }
}
