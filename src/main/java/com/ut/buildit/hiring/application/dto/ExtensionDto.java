package com.ut.buildit.hiring.application.dto;

import lombok.Data;

@Data
public class ExtensionDto {

    private Long plantHireRequestId;
    private Long daysToExtend;
}
