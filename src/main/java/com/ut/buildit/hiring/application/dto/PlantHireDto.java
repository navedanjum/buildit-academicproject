package com.ut.buildit.hiring.application.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PlantHireDto extends ResourceSupport {

    private BigDecimal totalCost;
    private Long _id;
    private Long constructionSiteId;
    private String plantSupplier;
    private Long plantId;
    private String plantName;
    private BigDecimal plantPrice;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;
    private String siteEngineerName;
    private Long purchaseOrderId;
    private String status;
    private String serverName;
    private String comment;
}
