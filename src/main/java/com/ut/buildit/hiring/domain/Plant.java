package com.ut.buildit.hiring.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
public class Plant {

    @Id
    @GeneratedValue
    private Long id;

    private Long plantId;

    private String name;

    @Column(precision = 8, scale = 2)
    BigDecimal price;

    public Plant(Long plantId, String name, BigDecimal price) {
        this.plantId = plantId;
        this.name = name;
        this.price = price;
    }
}
