package com.ut.buildit.hiring.domain;

import com.ut.buildit.common.config.user.ApplicationUser;
import com.ut.buildit.common.domain.BusinessPeriod;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class PlantHireEntity {

    @Id
    @GeneratedValue
    Long id;
    @Column(precision = 8, scale = 2)
    BigDecimal totalCost;
    @ManyToOne(cascade = {CascadeType.PERSIST})
    private ConstructionSite constructionSite;
    private String plantSupplier;
    @ManyToOne(cascade = {CascadeType.PERSIST})
    private Plant plant;
    @Embedded
    private BusinessPeriod period;
    private String siteEngineerName;

    @OneToOne
    private PurchaseOrder purchaseOrder;

    @ManyToOne
    private ApplicationUser user;

    private String status;

    private String comment;

    private String serverName;

}
