package com.ut.buildit.hiring.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
@NoArgsConstructor
public class PurchaseOrder {

    @Id
    @GeneratedValue
    private Long id;

    private Long purchaseOrderId;

    @OneToOne
    private PlantHireEntity plantHireEntity;

    public PurchaseOrder(Long purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }
}
