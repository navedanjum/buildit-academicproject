package com.ut.buildit.hiring.rest;

import com.ut.buildit.hiring.application.dto.ExtensionDto;
import com.ut.buildit.hiring.application.dto.PlantHireDto;
import com.ut.buildit.hiring.application.service.PlantHireService;
import com.ut.buildit.hiring.domain.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/hire-plant")
@Slf4j
public class PlantHireController {

    @Autowired
    private PlantHireService plantHireService;


    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public PlantHireDto create(@RequestBody PlantHireDto plantHireDto) {
        return plantHireService.savePlantHireRequest(plantHireDto);
    }

    @GetMapping("/{id}")
    public PlantHireDto get(@PathVariable Long id) {
        return plantHireService.getPlantHireRequest(id);
    }

    @PutMapping("/{id}")
    public PlantHireDto update(@PathVariable Long id, @RequestBody PlantHireDto plantHireDto) {
        plantHireDto.set_id(id);
        return plantHireService.updatePlantHireRequest(plantHireDto);
    }

    @PostMapping("/{id}/{status}/{serverName}")
    public void updateStatus(@PathVariable Long id, @PathVariable Status status, @PathVariable String serverName, @RequestBody String comment) {
        plantHireService.updateStatus(status, id, comment, serverName);
    }

    @PutMapping("/update-PO/{poId}/{status}")
    public PlantHireDto setPurchaseOrder(@PathVariable Long poId, @PathVariable Status status) {
        log.warn("update purchase order, status {}, purchase order {}", status.name(), poId);
        return plantHireService.updateBasedOnPurchaseOrderStatus(poId, status);
    }

    @GetMapping("/all/{constructionId}")
    public List<PlantHireDto> getAll(@PathVariable Long constructionId) {
        return plantHireService.getAllHires(constructionId);
    }

    @PostMapping("/extend/{serverName}")
    public void extendPhr(@PathVariable String serverName, @RequestBody ExtensionDto extensionDto){
        plantHireService.extendPhr(extensionDto, serverName);
    }
}
