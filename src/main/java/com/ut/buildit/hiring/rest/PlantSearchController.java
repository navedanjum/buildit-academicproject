package com.ut.buildit.hiring.rest;

import com.ut.buildit.common.service.RentItDaoLocator;
import com.ut.buildit.hiring.application.dto.PlantDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/api/plant/search/{serverName}")
public class PlantSearchController {

    @Autowired
    private RentItDaoLocator locator;

    @GetMapping
    public List<PlantDto> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") String startDate,
            @RequestParam(name = "endDate") String endDate,
            @PathVariable String serverName) {
        return locator.getRentItDao(serverName).getPlants(plantName, startDate, endDate);
    }


}
