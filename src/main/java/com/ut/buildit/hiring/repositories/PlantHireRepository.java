package com.ut.buildit.hiring.repositories;

import com.ut.buildit.hiring.domain.PlantHireEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantHireRepository extends JpaRepository<PlantHireEntity, Long> {

    List<PlantHireEntity> findByConstructionSite_Id(Long constructionSiteId);

    PlantHireEntity findByPurchaseOrder_PurchaseOrderId(Long purchaseOrderId);
}
