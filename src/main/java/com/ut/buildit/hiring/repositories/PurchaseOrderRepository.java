package com.ut.buildit.hiring.repositories;

import com.ut.buildit.hiring.domain.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {

    PurchaseOrder findByPurchaseOrderId(Long purchaseOrderId);
}
