package com.ut.buildit.hiring.repositories;

import com.ut.buildit.hiring.domain.ConstructionSite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConstructionSiteRepository extends JpaRepository<ConstructionSite, Long> {
}
