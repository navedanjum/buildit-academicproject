package com.ut.buildit.hiring.repositories;

import com.ut.buildit.hiring.domain.Plant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlantRepository extends JpaRepository<Plant, Long> {

    Plant findByPlantId(Long plantId);
}
