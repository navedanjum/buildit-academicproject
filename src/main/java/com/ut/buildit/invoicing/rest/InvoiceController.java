package com.ut.buildit.invoicing.rest;

import com.ut.buildit.hiring.application.dto.PurchaseOrderDto;
import com.ut.buildit.invoicing.application.dto.InvoiceDto;
import com.ut.buildit.invoicing.application.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/api/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;


    @GetMapping("/all/{serverName}")
    public List<InvoiceDto> getAllInvoices(@PathVariable String serverName) {
        return invoiceService.getAllInvoices(serverName);
    }

    @GetMapping("/{id}/{serverName}")
    public InvoiceDto getInvoice(@PathVariable Long purchaseOrderId, @PathVariable String serverName) {
        return invoiceService.getInvoice(purchaseOrderId, serverName);
    }

    @PostMapping("/remittence/{purchaseOrderId}/{serverName}")
    public void remittence(@PathVariable Long purchaseOrderId, @PathVariable String serverName) {
        invoiceService.remittance(purchaseOrderId, serverName);
    }

    @GetMapping("/purchaseOrder/{purchaseOrderId}/{serverName}")
    public PurchaseOrderDto getPurchaseOrder(@PathVariable Long purchaseOrder, @PathVariable String serverName) {
        return invoiceService.getPurchaseOrderDetail(purchaseOrder, serverName);
    }
}
