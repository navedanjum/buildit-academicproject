package com.ut.buildit.invoicing.application.dto;

import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class InvoiceDto extends ResourceSupport {

    private Long _id;
    boolean paid;
    LocalDate dueDate;
    LocalDate paidDate;
    BigDecimal amount;
    Long purchaseOrderId;
}
