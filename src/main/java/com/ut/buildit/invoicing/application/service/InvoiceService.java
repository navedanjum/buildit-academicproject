package com.ut.buildit.invoicing.application.service;

import com.ut.buildit.hiring.application.dto.PurchaseOrderDto;
import com.ut.buildit.invoicing.application.dto.InvoiceDto;

import java.util.List;

public interface InvoiceService {

    List<InvoiceDto> getAllInvoices(String serverName);
    InvoiceDto getInvoice(Long purchaseOrderId, String serverName);
    void remittance(Long purchaseOrderId, String serverName);
    PurchaseOrderDto getPurchaseOrderDetail(Long purchaseOrderId, String serverName);
}
