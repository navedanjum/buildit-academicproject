package com.ut.buildit.invoicing.application.service;

import com.ut.buildit.common.service.RentItDaoLocator;
import com.ut.buildit.hiring.application.dto.PurchaseOrderDto;
import com.ut.buildit.invoicing.application.dto.InvoiceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private RentItDaoLocator locator;

    @Override
    public List<InvoiceDto> getAllInvoices(String serverName) {
        return locator.getRentItDao(serverName).getAllInvoices();
    }

    @Override
    public InvoiceDto getInvoice(Long purchaseOrderId, String serverName) {
        return locator.getRentItDao(serverName).getInvoice(purchaseOrderId);
    }

    @Override
    public void remittance(Long purchaseOrderId, String serverName) {
        locator.getRentItDao(serverName).remittence(purchaseOrderId);
    }

    @Override
    public PurchaseOrderDto getPurchaseOrderDetail(Long purchaseOrderId, String serverName) {
        return locator.getRentItDao(serverName).getPurchaseOrder(purchaseOrderId);
    }
}
