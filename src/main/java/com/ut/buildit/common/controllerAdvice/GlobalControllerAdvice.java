package com.ut.buildit.common.controllerAdvice;

import com.ut.buildit.common.dto.BaseResponse;
import com.ut.buildit.common.exception.PhrNotFoundException;
import com.ut.buildit.common.exception.PhrStatusChangeException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.URISyntaxException;

@RestControllerAdvice
public class GlobalControllerAdvice {


    @ExceptionHandler(PhrNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public BaseResponse HandlePhrNotFoundException(PhrNotFoundException ex) {
        BaseResponse baseResponse = new BaseResponse(ex.getMessage());
        return baseResponse;
    }

    @ExceptionHandler(PhrStatusChangeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseResponse HandlePhrStatusChangeException(PhrStatusChangeException ex) {
        BaseResponse baseResponse = new BaseResponse(ex.getMessage());
        return baseResponse;
    }

    @ExceptionHandler(SecurityException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseResponse HandleSecirityException(SecurityException ex) {
        BaseResponse baseResponse = new BaseResponse(ex.getMessage());
        return baseResponse;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseResponse HandleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        BaseResponse baseResponse = new BaseResponse(bindingResult.getFieldError().getCode());
        return baseResponse;
    }

    @ExceptionHandler(URISyntaxException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public BaseResponse HandleURISyntaxException(URISyntaxException ex) {
        BaseResponse baseResponse = new BaseResponse("Something bad has happened");
        return baseResponse;
    }
}
