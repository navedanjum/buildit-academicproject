package com.ut.buildit.common.exception;

public class PhrStatusChangeException extends RuntimeException {

    public PhrStatusChangeException(String message) {
        super(message);
    }
}
