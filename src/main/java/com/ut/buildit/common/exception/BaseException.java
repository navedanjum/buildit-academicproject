package com.ut.buildit.common.exception;

public class BaseException extends RuntimeException {

    public BaseException(String message) {
        super(message);
    }
}
