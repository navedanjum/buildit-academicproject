package com.ut.buildit.common.exception;

public class SecurityException extends RuntimeException {

    public SecurityException(String message) {
        super(message);
    }
}
