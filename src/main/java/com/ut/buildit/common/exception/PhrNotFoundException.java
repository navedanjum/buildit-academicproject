package com.ut.buildit.common.exception;

public class PhrNotFoundException extends RuntimeException {

    public PhrNotFoundException(Long id) {
        super("No phr found with id"+id);
    }
}
