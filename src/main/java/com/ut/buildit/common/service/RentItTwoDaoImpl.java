package com.ut.buildit.common.service;

import com.ut.buildit.common.dto.BaseResponse;
import com.ut.buildit.hiring.application.dto.BusinessPeriodDTO;
import com.ut.buildit.hiring.application.dto.ExtensionDto;
import com.ut.buildit.hiring.application.dto.PlantDto;
import com.ut.buildit.hiring.application.dto.PurchaseOrderDto;
import com.ut.buildit.hiring.domain.PlantHireEntity;
import com.ut.buildit.hiring.domain.PurchaseOrder;
import com.ut.buildit.hiring.domain.Status;
import com.ut.buildit.hiring.repositories.PlantHireRepository;
import com.ut.buildit.hiring.repositories.PurchaseOrderRepository;
import com.ut.buildit.invoicing.application.dto.InvoiceDto;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("Two")
@Slf4j
public class RentItTwoDaoImpl implements RentItDao {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private PlantHireRepository plantHireRepository;

    private static String baseUrl = "http://localhost:9091";

    @Override
    public List<PlantDto> getPlants(String plantName, String startDate, String endDate) {
        log.debug("calling to restIf to get plants");
        String url = baseUrl + "/api/sales/plants?name=" + plantName + "&startDate=" + startDate + "&endDate=" + endDate;
        PlantDto[] plantDtos = restTemplate.getForObject(url, PlantDto[].class);
        log.debug("search result: {}", plantDtos);
        if (plantDtos != null) {
            return Arrays.asList(plantDtos);
        }
        return new ArrayList<>();
    }

    @Override
    public void createPurchaseOrder(PlantHireEntity plantHireEntity) {

        log.debug("Calling to create purchase order");
        String url = baseUrl + "/api/sales/orders";
        PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
        PlantDto plantDto = new PlantDto();
        plantDto.set_id(plantHireEntity.getPlant().getPlantId());
        plantDto.setName(plantHireEntity.getPlant().getName());
        purchaseOrderDto.setPlant(plantDto);
        purchaseOrderDto.setRentalPeriod(BusinessPeriodDTO.of(plantHireEntity.getPeriod().getStartDate(), plantHireEntity.getPeriod().getEndDate()));
        purchaseOrderDto.setUserEmail(plantHireEntity.getUser().getUserEmail());
        log.warn("sending request: {}", purchaseOrderDto);
        purchaseOrderDto = restTemplate.postForObject(url, purchaseOrderDto, PurchaseOrderDto.class);
        log.warn("response of purchase order: {}", purchaseOrderDto);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findByPurchaseOrderId(purchaseOrderDto.get_id());
        if (purchaseOrder == null) {
            purchaseOrder = purchaseOrderRepository.saveAndFlush(new PurchaseOrder(purchaseOrderDto.get_id()));
        }
        log.warn("purchase order: {}", purchaseOrder);
        plantHireEntity.setPurchaseOrder(purchaseOrder);
    }

    @Async
    @Override
    public void CancelPlantHireRequest(PlantHireEntity plantHireEntity) {
        log.debug("Calling to cancel order");
        String url = baseUrl + "/api/sales/orders/" + plantHireEntity.getPurchaseOrder().getPurchaseOrderId() + "/customer_rejection";
        restTemplate.getForObject(url, Void.class);
    }

    @Async
    @Override
    public void ExtendPO(ExtensionDto extensionDto, Long purchaseOrderId) {
        log.debug("Calling to extension order");
        String url = baseUrl + "/api/sales//orders/"+purchaseOrderId+"/extensions";
        PlantHireEntity plantHireEntity = plantHireRepository.findByPurchaseOrder_PurchaseOrderId(purchaseOrderId);
        Extension extension = new Extension();
        extension.setEndDate(plantHireEntity.getPeriod().getEndDate().plusDays(extensionDto.getDaysToExtend()));
        HttpEntity<Extension> request = new HttpEntity<>(extension);
        restTemplate.exchange(url, HttpMethod.POST ,request, BaseResponse.class);
    }

    @Override
    public PurchaseOrderDto getPurchaseOrder(Long purchaseOrderId) {
        log.debug("getting purchase order information");
        String url = baseUrl + "/api/sales/orders/"+purchaseOrderId;
        PurchaseOrderDto purchaseOrderDto = restTemplate.getForObject(url, PurchaseOrderDto.class);
        log.debug("search result: {}", purchaseOrderDto);
        return purchaseOrderDto;
    }

    @Override
    public List<InvoiceDto> getAllInvoices() {
        log.debug("getting All invoices");
        String url = baseUrl + "/api/invoices/all/";
        InvoiceDto[]invoiceArray = restTemplate.getForObject(url, InvoiceDto[].class);
        log.debug("search result: {}", invoiceArray);
        if(invoiceArray != null) {
            return Arrays.asList(invoiceArray);
        }
        return new ArrayList<>();
    }

    @Override
    public InvoiceDto getInvoice(Long purchaseOrderId) {
        log.debug("getting Invoice for purchase order: {}", purchaseOrderId);
        String url = baseUrl + "/api/invoices/"+purchaseOrderId;
        InvoiceDto invoice = restTemplate.getForObject(url, InvoiceDto.class);
        log.debug("search result: {}", invoice);
        return invoice;
    }

    @Override
    public void remittence(Long purchaseOrderId) {
        log.debug("Sending remittence advice");
        String url = "http://localhost:8081/api/invoices/"+ purchaseOrderId + "/acceptance";
        restTemplate.postForObject(url, null, Void.class);
    }

    @Data
    private class Extension {
        LocalDate endDate;
    }
}
