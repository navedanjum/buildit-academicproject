package com.ut.buildit.common.service;

import com.ut.buildit.common.dto.BaseResponse;
import com.ut.buildit.hiring.application.dto.BusinessPeriodDTO;
import com.ut.buildit.hiring.application.dto.ExtensionDto;
import com.ut.buildit.hiring.application.dto.PlantDto;
import com.ut.buildit.hiring.application.dto.PurchaseOrderDto;
import com.ut.buildit.hiring.domain.PlantHireEntity;
import com.ut.buildit.hiring.domain.PurchaseOrder;
import com.ut.buildit.hiring.domain.Status;
import com.ut.buildit.hiring.repositories.PurchaseOrderRepository;
import com.ut.buildit.invoicing.application.dto.InvoiceDto;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service("Core")
public class RentItDaoImpl implements RentItDao {

    @Value("${rentIt.secret}")
    String secret;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private ApplicationUserService applicationUserService;

    private MultiValueMap<String, String> headers;

    @PostConstruct
    public void init() throws Exception {
        headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", secret);
    }

    @Override
    public List<PlantDto> getPlants(String plantName, String startDate, String endDate) {
        log.debug("calling to restIf to get plants");
        String url = "http://localhost:8081/api/sales/plants?name=" + plantName + "&startDate=" + startDate + "&endDate=" + endDate;
        HttpEntity<?> entity = new HttpEntity<String>(headers);
        ResponseEntity<PlantDto[]> plantDtos = restTemplate.exchange(url, HttpMethod.GET, entity, PlantDto[].class);
        log.debug("search result: {}", plantDtos);
        if (plantDtos != null) {
            return Arrays.asList(plantDtos.getBody());
        }
        return new ArrayList<>();
    }

    @Override
    public void createPurchaseOrder(PlantHireEntity plantHireEntity) {

        log.debug("Calling to create purchase order");
        String url = "http://localhost:8081/api/sales/orders";
        PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
        PlantDto plantDto = new PlantDto();
        plantDto.set_id(plantHireEntity.getPlant().getPlantId());
        plantDto.setName(plantHireEntity.getPlant().getName());
        purchaseOrderDto.setPlant(plantDto);
        purchaseOrderDto.setRentalPeriod(BusinessPeriodDTO.of(plantHireEntity.getPeriod().getStartDate(), plantHireEntity.getPeriod().getEndDate()));
        purchaseOrderDto.setUserEmail(plantHireEntity.getUser().getUserEmail());
        log.warn("sending request: {}", purchaseOrderDto);
        HttpEntity<PurchaseOrderDto> request = new HttpEntity<>(purchaseOrderDto, headers);
        purchaseOrderDto = restTemplate.postForObject(url, request, PurchaseOrderDto.class);
        log.warn("response of purchase order: {}", purchaseOrderDto);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findByPurchaseOrderId(purchaseOrderDto.get_id());
        if (purchaseOrder == null) {
            purchaseOrder = purchaseOrderRepository.saveAndFlush(new PurchaseOrder(purchaseOrderDto.get_id()));
        }
        log.warn("purchase order: {}", purchaseOrder);
        plantHireEntity.setPurchaseOrder(purchaseOrder);
    }

    @Async
    @Override
    public void CancelPlantHireRequest(PlantHireEntity plantHireEntity) {
        log.debug("Calling to cancel order");
        String url = "http://localhost:8081/api/sales/orders/update/" + plantHireEntity.getPurchaseOrder().getPurchaseOrderId() + "/" + Status.CANCELED_BY_CLIENT;
        HttpEntity<?> request = new HttpEntity<>(headers);
        restTemplate.postForObject(url, request, Void.class);
    }

    @Async
    @Override
    public void ExtendPO(ExtensionDto extensionDto, Long purchaseOrderId) {
        log.debug("Calling to extension order");
        ExtendOrderRequest extendOrderRequest = new ExtendOrderRequest();
        extendOrderRequest.setDaysToExtend(extensionDto.getDaysToExtend());
        extendOrderRequest.setPurchaseOrderId(purchaseOrderId);
        String url = "http://localhost:8081/api/sales//orders/extend";
        HttpEntity<ExtendOrderRequest> request = new HttpEntity<>(extendOrderRequest, headers);
        restTemplate.exchange(url, HttpMethod.POST ,request, BaseResponse.class);
    }

    @Override
    public PurchaseOrderDto getPurchaseOrder(Long purchaseOrderId) {
        log.debug("getting purchase order information");
        String url = "http://localhost:8081/api/sales/orders/"+purchaseOrderId;
        HttpEntity<?> entity = new HttpEntity<String>(headers);
        ResponseEntity<PurchaseOrderDto> purchaseOrderDto = restTemplate.exchange(url, HttpMethod.GET, entity, PurchaseOrderDto.class);
        log.debug("search result: {}", purchaseOrderDto);
        return purchaseOrderDto.getBody();
    }

    @Override
    public List<InvoiceDto> getAllInvoices() {
        log.debug("getting All invoices");
        String url = "http://localhost:8081//api/invoice/all/"+applicationUserService.findByUser().getUserEmail();
        HttpEntity<?> entity = new HttpEntity<String>(headers);
        ResponseEntity<InvoiceDto[]> invoiceArray = restTemplate.exchange(url, HttpMethod.GET, entity, InvoiceDto[].class);
        log.debug("search result: {}", invoiceArray);
        if(invoiceArray != null) {
            return Arrays.asList(invoiceArray.getBody());
        }
        return new ArrayList<>();
    }

    @Override
    public InvoiceDto getInvoice(Long purchaseOrderId) {
        log.debug("getting Invoice for purchase order: {}", purchaseOrderId);
        String url = "http://localhost:8081/api/invoice/"+purchaseOrderId;
        HttpEntity<?> entity = new HttpEntity<String>(headers);
        ResponseEntity<InvoiceDto> invoice = restTemplate.exchange(url, HttpMethod.GET, entity, InvoiceDto.class);
        log.debug("search result: {}", invoice);
        return invoice.getBody();
    }

    @Override
    public void remittence(Long purchaseOrderId) {
        log.debug("Sending remittence advice");
        String url = "http://localhost:8081/api/invoice/remittance/"+ purchaseOrderId;
        HttpEntity<?> request = new HttpEntity<>(headers);
        restTemplate.postForObject(url, request, Void.class);
    }

    @Data
    private class ExtendOrderRequest {
        private Long purchaseOrderId;
        private Long daysToExtend;
    }
}
