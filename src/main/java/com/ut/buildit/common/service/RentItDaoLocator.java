package com.ut.buildit.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class RentItDaoLocator {

    @Autowired
    @Qualifier("Core")
    private RentItDao coreRentIt;

    @Autowired
    @Qualifier("One")
    private RentItDao rentItOne;

    @Autowired
    @Qualifier("Two")
    private RentItDao rentItTwo;

    public RentItDao getRentItDao(String name) {
        switch (name){
            case "Core":
                return this.coreRentIt;
            case "One":
                return this.rentItOne;
            case "Two":
                return this.rentItTwo;
            default:
                return this.coreRentIt;
        }
    }
}
