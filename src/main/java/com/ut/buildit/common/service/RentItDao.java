package com.ut.buildit.common.service;

import com.ut.buildit.hiring.application.dto.ExtensionDto;
import com.ut.buildit.hiring.application.dto.PlantDto;
import com.ut.buildit.hiring.application.dto.PurchaseOrderDto;
import com.ut.buildit.hiring.domain.PlantHireEntity;
import com.ut.buildit.invoicing.application.dto.InvoiceDto;

import java.util.List;

public interface RentItDao {

    List<PlantDto> getPlants(String plantName, String startDate, String endDate);

    void createPurchaseOrder(PlantHireEntity plantHireEntity);

    void CancelPlantHireRequest(PlantHireEntity plantHireEntity);

    void ExtendPO(ExtensionDto extensionDto, Long purchaseOrderId);

    PurchaseOrderDto getPurchaseOrder(Long purchaseOrderId);

    List<InvoiceDto> getAllInvoices();

    InvoiceDto getInvoice(Long purchaseOrderId);

    void remittence(Long purchaseOrderId);
}
