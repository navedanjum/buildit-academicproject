package com.ut.buildit.common.service;

import com.ut.buildit.common.config.user.ApplicationUser;
import com.ut.buildit.common.config.user.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("test")
@Service
public class MockApplicationUserServiceImpl implements ApplicationUserService {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Override
    public ApplicationUser findByUser() {

        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setPassword("password");
        applicationUser.setUsername("test");
        applicationUser.setUserEmail("abc9991122@gmail.com");
        applicationUser = applicationUserRepository.save(applicationUser);
        return applicationUser;
    }
}
