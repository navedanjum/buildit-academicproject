package com.ut.buildit.common.service;

import com.ut.buildit.common.config.user.ApplicationUser;
import com.ut.buildit.common.config.user.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Profile(value = {"dev", "live"})
@Service
public class ApplicationUserServiceImpl implements ApplicationUserService {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Override
    public ApplicationUser findByUser() {
        ApplicationUser applicationUser = applicationUserRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        if(applicationUser == null) {
            throw new SecurityException("Now user found at context");
        }
        return applicationUser;
    }
}
