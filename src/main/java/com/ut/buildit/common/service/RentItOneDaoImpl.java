package com.ut.buildit.common.service;

import com.ut.buildit.common.dto.BaseResponse;
import com.ut.buildit.hiring.application.dto.BusinessPeriodDTO;
import com.ut.buildit.hiring.application.dto.ExtensionDto;
import com.ut.buildit.hiring.application.dto.PlantDto;
import com.ut.buildit.hiring.application.dto.PurchaseOrderDto;
import com.ut.buildit.hiring.domain.PlantHireEntity;
import com.ut.buildit.hiring.domain.PurchaseOrder;
import com.ut.buildit.hiring.repositories.PlantHireRepository;
import com.ut.buildit.hiring.repositories.PurchaseOrderRepository;
import com.ut.buildit.invoicing.application.dto.InvoiceDto;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service("One")
@Slf4j
public class RentItOneDaoImpl implements RentItDao {

    private static final String baseUrl = "https://localhost:8090";

    @Value("${rentIt.secret.one}")
    String secret;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;;

    @Autowired
    private PlantHireRepository plantHireRepository;

    private MultiValueMap<String, String> headers;

    @PostConstruct
    public void init() throws Exception {
        headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", secret);
    }

    @Override
    public List<PlantDto> getPlants(String plantName, String startDate, String endDate) {
        log.warn("calling to restIf to get plants from one dao");
        String url = baseUrl + "/api/sales/plants?name=" + plantName + "&startDate=" + startDate + "&endDate=" + endDate;
        PlantListResponse plantDtos = restTemplate.getForObject(url, PlantListResponse.class);
        log.debug("search result: {}", plantDtos);
        if (plantDtos != null) {
            return plantDtos._embedded.plants;
        }
        return new ArrayList<>();
    }

    @Override
    public void createPurchaseOrder(PlantHireEntity plantHireEntity) {

        log.debug("Calling to create purchase order");
        String url = baseUrl + "/api/sales/orders";
        PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
        PlantDto plantDto = new PlantDto();
        plantDto.set_id(plantHireEntity.getPlant().getPlantId());
        plantDto.setName(plantHireEntity.getPlant().getName());
        purchaseOrderDto.setPlant(plantDto);
        purchaseOrderDto.setRentalPeriod(BusinessPeriodDTO.of(plantHireEntity.getPeriod().getStartDate(), plantHireEntity.getPeriod().getEndDate()));
        purchaseOrderDto.setUserEmail(plantHireEntity.getUser().getUserEmail());
        log.warn("sending request: {}", purchaseOrderDto);
        HttpEntity<PurchaseOrderDto> request = new HttpEntity<>(purchaseOrderDto, headers);
        purchaseOrderDto = restTemplate.postForObject(url, request, PurchaseOrderDto.class);
        log.warn("response of purchase order: {}", purchaseOrderDto);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findByPurchaseOrderId(purchaseOrderDto.get_id());
        if (purchaseOrder == null) {
            purchaseOrder = purchaseOrderRepository.saveAndFlush(new PurchaseOrder(purchaseOrderDto.get_id()));
        }
        log.warn("purchase order: {}", purchaseOrder);
        plantHireEntity.setPurchaseOrder(purchaseOrder);
    }

    @Async
    @Override
    public void CancelPlantHireRequest(PlantHireEntity plantHireEntity) {
        log.debug("Calling to cancel order");
        String url = baseUrl+"api/sales/orders/update/" + plantHireEntity.getPurchaseOrder().getPurchaseOrderId() + "/cancel";
        HttpEntity<?> request = new HttpEntity<>(headers);
        restTemplate.postForObject(url, request, Void.class);
    }

    @Async
    @Override
    public void ExtendPO(ExtensionDto extensionDto, Long purchaseOrderId) {
        log.debug("Calling to extension order");
        ExtendOrderRequest extendOrderRequest = new ExtendOrderRequest();
        extendOrderRequest.setDaysToExtend(extensionDto.getDaysToExtend());
        extendOrderRequest.setPurchaseOrderId(purchaseOrderId);
        PlantHireEntity plantHireEntity = plantHireRepository.findByPurchaseOrder_PurchaseOrderId(extensionDto.getPlantHireRequestId());
        PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
        PlantDto plantDto = new PlantDto();
        plantDto.set_id(plantHireEntity.getPlant().getPlantId());
        purchaseOrderDto.setPlant(new PlantDto());
        purchaseOrderDto.setRentalPeriod(BusinessPeriodDTO.of(plantHireEntity.getPeriod().getStartDate(),
                plantHireEntity.getPeriod().getEndDate().plusDays(extensionDto.getDaysToExtend())));
        String url = baseUrl + "/api/sales//orders/"+purchaseOrderId+"/extension";
        HttpEntity<PurchaseOrderDto> request = new HttpEntity<>(purchaseOrderDto, headers);
        restTemplate.exchange(url, HttpMethod.POST, request, BaseResponse.class);
    }

    @Override
    public PurchaseOrderDto getPurchaseOrder(Long purchaseOrderId) {
        log.debug("getting purchase order information");
        String url = baseUrl+"/api/sales/orders/" + purchaseOrderId;
        HttpEntity<?> entity = new HttpEntity<String>(headers);
        ResponseEntity<PurchaseOrderDto> purchaseOrderDto = restTemplate.exchange(url, HttpMethod.GET, entity, PurchaseOrderDto.class);
        log.debug("search result: {}", purchaseOrderDto);
        return purchaseOrderDto.getBody();
    }

    @Override
    public List<InvoiceDto> getAllInvoices() {


        // No api inplemented by this group


//        log.debug("getting All invoices");
//        String url = "http://localhost:8081//api/invoice/all/" + applicationUserService.findByUser().getUserEmail();
//        HttpEntity<?> entity = new HttpEntity<String>(headers);
//        ResponseEntity<InvoiceDto[]> invoiceArray = restTemplate.exchange(url, HttpMethod.GET, entity, InvoiceDto[].class);
//        log.debug("search result: {}", invoiceArray);
//        if (invoiceArray != null) {
//            return Arrays.asList(invoiceArray.getBody());
//        }
        return new ArrayList<>();
    }

    @Override
    public InvoiceDto getInvoice(Long purchaseOrderId) {

        // No apu implemented by this group

//        log.debug("getting Invoice for purchase order: {}", purchaseOrderId);
//        String url = "http://localhost:8081/api/invoice/" + purchaseOrderId;
//        HttpEntity<?> entity = new HttpEntity<String>(headers);
//        ResponseEntity<InvoiceDto> invoice = restTemplate.exchange(url, HttpMethod.GET, entity, InvoiceDto.class);
//        log.debug("search result: {}", invoice);
//        return invoice.getBody();
        return new InvoiceDto();
    }

    @Override
    public void remittence(Long purchaseOrderId) {
        log.debug("Sending remittence advice");
        String url = "http://localhost:8081/api/invoice/remittance/" + purchaseOrderId;
        RemmitenceAdviceDto remmitenceAdviceDto = new RemmitenceAdviceDto();
        remmitenceAdviceDto.setAmount(getInvoice(purchaseOrderId).getAmount());
        remmitenceAdviceDto.setPoID(purchaseOrderId);
        HttpEntity<RemmitenceAdviceDto> request = new HttpEntity<>(remmitenceAdviceDto, headers);
        restTemplate.postForObject(url, request, Void.class);
    }

    @Data
    private class ExtendOrderRequest {
        private Long purchaseOrderId;
        private Long daysToExtend;
    }

    @Data
    private class RemmitenceAdviceDto {
        private Long poID;
        private BigDecimal amount;
        private LocalDate dueDate;
    }

    @Data
    public class PlantListResponse {
        _embedded _embedded;


        // Getter Methods

        public _embedded get_embedded() {
            return _embedded;
        }

        // Setter Methods

        public void set_embedded(_embedded _embeddedObject) {
            this._embedded = _embeddedObject;
        }
    }
    public class _embedded {
        ArrayList < PlantDto > plants = new ArrayList <> ();

        public ArrayList<PlantDto> getPlants() {
            return plants;
        }

        public void setPlants(ArrayList<PlantDto> plants) {
            this.plants = plants;
        }
    }
}
