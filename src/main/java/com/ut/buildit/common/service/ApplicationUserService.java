package com.ut.buildit.common.service;

import com.ut.buildit.common.config.user.ApplicationUser;

public interface ApplicationUserService {

    ApplicationUser findByUser();
}
