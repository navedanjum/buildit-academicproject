package com.ut.buildit;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ut.buildit.common.config.security.WebSecurity;
import com.ut.buildit.common.config.user.ApplicationUser;
import com.ut.buildit.hiring.application.dto.PlantHireDto;
import com.ut.buildit.hiring.domain.Status;
import com.ut.buildit.hiring.repositories.PlantHireRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {BuilditApplication.class, WebSecurity.class})
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InvoiceControllerTest {
    @Autowired
    PlantHireRepository repo;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Before public void setup() {this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();}


    @Test
    public void testCreate() throws Exception {

    }


    }

