package com.ut.buildit;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ut.buildit.common.config.security.WebSecurity;
import com.ut.buildit.common.config.user.ApplicationUser;
import com.ut.buildit.hiring.application.dto.PlantHireDto;
import com.ut.buildit.hiring.domain.Status;
import com.ut.buildit.hiring.repositories.PlantHireRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {BuilditApplication.class, WebSecurity.class})
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlantHireControllerTest {
    @Autowired
    PlantHireRepository repo;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Before public void setup() {this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();}

    @Test
    public void testCreate() throws Exception {

        PlantHireDto plantHireDto = new PlantHireDto();
        plantHireDto.setConstructionSiteId(1L);
        plantHireDto.setPlantSupplier("max");
        plantHireDto.setPlantName("truck");
        plantHireDto.setPlantPrice(BigDecimal.valueOf(222));
        plantHireDto.setStartDate(LocalDate.now());
        plantHireDto.setEndDate(LocalDate.now().plusDays(2));
        plantHireDto.setSiteEngineerName("john");

        MvcResult result = mockMvc.perform(post("/api/hire-plant/create").content(mapper.writeValueAsString(plantHireDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PlantHireDto createdPlantHireDto = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PlantHireDto>() {
        });
        assertThat(createdPlantHireDto.getPlantName()).isEqualTo(plantHireDto.getPlantName());
        assertThat(createdPlantHireDto.getPlantSupplier()).isEqualTo(plantHireDto.getPlantSupplier());
        assertThat(createdPlantHireDto.getPlantPrice()).isEqualTo(plantHireDto.getPlantPrice());
        assertThat(createdPlantHireDto.getSiteEngineerName()).isEqualTo(plantHireDto.getSiteEngineerName());


    }

    @Test
    public void testGet() throws Exception {
        PlantHireDto plantHireDto = new PlantHireDto();
        plantHireDto.setConstructionSiteId(1L);
        plantHireDto.setPlantSupplier("max");
        plantHireDto.setPlantName("truck");
        plantHireDto.setPlantPrice(BigDecimal.valueOf(222));
        plantHireDto.setStartDate(LocalDate.now());
        plantHireDto.setEndDate(LocalDate.now().plusDays(2));
        plantHireDto.setSiteEngineerName("john");

        MvcResult result = mockMvc.perform(post("/api/hire-plant/create").content(mapper.writeValueAsString(plantHireDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PlantHireDto createdPlantHireDto = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PlantHireDto>() {
        });

        result = mockMvc.perform(get("/api/hire-plant/" + createdPlantHireDto.get_id()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        assertThat(plantHireDto.getPlantName()).isEqualTo(createdPlantHireDto.getPlantName());
        assertThat(plantHireDto.getPlantSupplier()).isEqualTo(createdPlantHireDto.getPlantSupplier());
        assertThat(plantHireDto.getSiteEngineerName()).isEqualTo(createdPlantHireDto.getSiteEngineerName());

    }

   @Test
    public void testUpdateStatus() throws Exception {

       PlantHireDto plantHireDto = new PlantHireDto();
       plantHireDto.setConstructionSiteId(1L);
       plantHireDto.setPlantSupplier("max");
       plantHireDto.setPlantName("truck");
       plantHireDto.setPlantPrice(BigDecimal.valueOf(222));
       plantHireDto.setStartDate(LocalDate.now());
       plantHireDto.setEndDate(LocalDate.now().plusDays(2));
       plantHireDto.setSiteEngineerName("john");

       MvcResult result = mockMvc.perform(post("/api/hire-plant/create").content(mapper.writeValueAsString(plantHireDto)).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated()).andReturn();
       PlantHireDto createdPlantHireDto = mapper.readValue(result.getResponse()
               .getContentAsString(), new TypeReference<PlantHireDto>() {

       });
       createdPlantHireDto.setSiteEngineerName("jack");

       result = mockMvc.perform(put("/api/hire-plant/" + createdPlantHireDto.get_id()).content(mapper.writeValueAsString(createdPlantHireDto)).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk()).andReturn();

        assertThat(createdPlantHireDto.getSiteEngineerName()).isEqualTo("jack");

    }

    @Test
    public void testGetAll() throws Exception {
        PlantHireDto plantHireDto = new PlantHireDto();
        plantHireDto.setConstructionSiteId(1L);
        plantHireDto.setPlantSupplier("max");
        plantHireDto.setPlantName("truck");
        plantHireDto.setPlantPrice(BigDecimal.valueOf(222));
        plantHireDto.setStartDate(LocalDate.now());
        plantHireDto.setEndDate(LocalDate.now().plusDays(2));
        plantHireDto.setSiteEngineerName("john");

        MvcResult result = mockMvc.perform(post("/api/hire-plant/create").content(mapper.writeValueAsString(plantHireDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PlantHireDto createdPlantHireDto = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PlantHireDto>() {
        });

         result = mockMvc.perform(get("/api/hire-plant/all/" + createdPlantHireDto.getConstructionSiteId()).content(mapper.writeValueAsString(plantHireDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

         assertThat(createdPlantHireDto.getConstructionSiteId()).isEqualTo(plantHireDto.getConstructionSiteId());
         assertThat(createdPlantHireDto.getPlantSupplier()).isEqualTo(plantHireDto.getPlantSupplier());

    }

   /* @Test
    public void testExtendPhr() throws Exception {

        PlantHireDto plantHireDto = new PlantHireDto();
        plantHireDto.setConstructionSiteId(1L);
        plantHireDto.setPlantSupplier("max");
        plantHireDto.setPlantName("truck");
        plantHireDto.setPlantPrice(BigDecimal.valueOf(222));
        plantHireDto.setStartDate(LocalDate.now());
        plantHireDto.setEndDate(LocalDate.now().plusDays(2));
        plantHireDto.setSiteEngineerName("john");
        plantHireDto.setServerName("server1");

        MvcResult result = mockMvc.perform(post("/api/hire-plant/create").content(mapper.writeValueAsString(plantHireDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PlantHireDto createdPlantHireDto = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PlantHireDto>() {
        });

        //createdPlantHireDto.setServerName("server2");
        result = mockMvc.perform(post("/api/hire-plant/extend/" + "server2").content(mapper.writeValueAsString(createdPlantHireDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        assertThat(createdPlantHireDto.getServerName()).isEqualTo("server2");




}*/


    @Test
    public void setPurchaseOrderTest() throws Exception {

        PlantHireDto plantHireDto = new PlantHireDto();
        plantHireDto.setConstructionSiteId(1L);
        plantHireDto.setPlantSupplier("max");
        plantHireDto.setPlantName("truck");
        plantHireDto.setPlantPrice(BigDecimal.valueOf(222));
        plantHireDto.setStartDate(LocalDate.now());
        plantHireDto.setEndDate(LocalDate.now().plusDays(2));
        plantHireDto.setSiteEngineerName("john");

        MvcResult result = mockMvc.perform(post("/api/hire-plant/create").content(mapper.writeValueAsString(plantHireDto)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
        PlantHireDto createdPlantHireDto = mapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<PlantHireDto>() {

        });
        //createdPlantHireDto.setStatus(Status.REJECTED);

        //result = mockMvc.perform(put("/api/hire-plant/update-PO/create" ).content(mapper.writeValueAsString(createdPlantHireDto)).contentType(MediaType.APPLICATION_JSON))
          //      .andExpect(status().isOk()).andReturn();

        //assertThat(createdPlantHireDto.getStatus()).isEqualTo(Status.APPROVED_BY_WORK_ENG);

    }




}

