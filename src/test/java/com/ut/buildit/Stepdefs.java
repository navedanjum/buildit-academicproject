package com.ut.buildit;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

public class Stepdefs {
    WebDriver porequest;
    RestTemplate restTemplate = new RestTemplate();

    static {
        System.setProperty("webdriver.chrome.driver", "c:/SWSETUP/chromedriver.exe");
    }

    @Before
    public void setup() {
        porequest = new ChromeDriver();
    }

    @After
    public void tearoff() {
        // Comment the following line if you want to check the application's final state on the browser
        porequest.close();
    }

}